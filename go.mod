module gitee.com/zxs-micro/zxs-micro-proto

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/pkg/errors v0.9.1
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
